/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.model;

import com.fasterxml.jackson.annotation.*;
import eu.elg.model.responses.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Representation of a successful completion response.
 *
 * @param <R> subclasses should instantiate this with their own type, similar to <code>java.lang.Enum</code>
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type"
)
@JsonSubTypes({
        @JsonSubTypes.Type(name = "stored", value = StoredResponse.class),
        @JsonSubTypes.Type(name = "annotations", value = AnnotationsResponse.class),
        @JsonSubTypes.Type(name = "classification", value = ClassificationResponse.class),
        @JsonSubTypes.Type(name = "texts", value = TextsResponse.class),
        @JsonSubTypes.Type(name = "audio", value = AudioResponse.class),
})
public class Response<R extends Response<R>> extends WarnForUnknownProperties {

  /**
   * A response may be successful but with warnings (for example if the input has only been partially processed, or if
   * the tool involves several sub-tasks and some of the tasks succeeded and others failed).
   */
  private List<StatusMessage> warnings;

  /**
   * Warning messages indicating the response was successful but with caveats.
   */
  @JsonProperty("warnings")
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  public List<StatusMessage> getWarnings() {
    return warnings;
  }

  /**
   * Set the list of warning messages that apply to this response.
   *
   * @param warnings warning messages.
   */
  @JsonProperty("warnings")
  public void setWarnings(List<StatusMessage> warnings) {
    this.warnings = warnings;
  }

  /**
   * Fluent setter for the {@link #getWarnings warnings property}
   *
   * @param warnings warning messages
   * @return this object, for chaining.
   */
  @SuppressWarnings("unchecked")
  public R withWarnings(List<StatusMessage> warnings) {
    setWarnings(warnings);
    return (R) this;
  }

  /**
   * Fluent setter for the {@link #getWarnings warnings property}, taking multiple parameters instead of a single List
   * to allow constructs like <code>new SomeResponse().withWarnings(new StatusMessage()....)</code>
   *
   * @param warnings warning messages
   * @return this object, for chaining
   */
  public R withWarnings(StatusMessage... warnings) {
    return withWarnings(Arrays.asList(warnings));
  }

  /**
   * Wrap this response in a message.
   *
   * @return a {@link ResponseMessage} wrapping this response
   */
  public ResponseMessage asMessage() {
    ResponseMessage msg = new ResponseMessage();
    msg.setResponse(this);
    return msg;
  }


  @Override
  protected Function<String, StatusMessage> propertyNameToMessage() {
    return StandardMessages::elgResponsePropertyUnsupported;
  }
}
