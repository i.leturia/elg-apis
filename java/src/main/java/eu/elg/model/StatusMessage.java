/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Represents a single status message, in a form amenable to internationalisation.  Each message contains a
 * <em>code</em>, which can be looked up in a list to find the actual text in any of the available languages.  The text
 * can contain numbered placeholders of the form <code>{0}</code>, which are filled in with values specified in the
 * "params" property.  The "text" property provides a single fallback text to be used if the specified code cannot be
 * found in the lookup table.
 */
public class StatusMessage {
  private String code;
  private List<String> params;
  private String text;
  private Map<String, Object> detail;

  /**
   * Error code for i18n lookup
   */
  @JsonProperty("code")
  public String getCode() {
    return code;
  }

  /**
   * Set the error code for i18n lookup.
   *
   * @param value new code
   */
  @JsonProperty("code")
  public void setCode(String value) {
    this.code = value;
  }

  /**
   * Fluent setter for the error code property
   *
   * @param code code to set
   * @return this object, for chaining
   */
  public StatusMessage withCode(String code) {
    setCode(code);
    return this;
  }

  /**
   * Parameters to fill in placeholders {0}, {1}, etc. in the "text"
   */
  @JsonProperty("params")
  public List<String> getParams() {
    return params;
  }

  /**
   * Set the parameters to fill in placeholders in the text.
   *
   * @param value parameter values.
   */
  @JsonProperty("params")
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public void setParams(List<String> value) {
    this.params = value;
  }

  /**
   * Fluent setter for the params property.
   *
   * @param params parameter values
   * @return this object, for chaining
   */
  public StatusMessage withParams(List<String> params) {
    setParams(params);
    return this;
  }

  /**
   * Fluent setter for the params property that accepts a varargs array rather than a List, allowing constructs like
   * <code>new StatusMessage().withText("Hello {0}, how are {1}").withParams("there", "you")</code>
   * @param params parameter values
   * @return this object, for chaining
   */
  public StatusMessage withParams(String... params) {
    return withParams(Arrays.asList(params));
  }

  /**
   * Fallback text of the message in the developer's preferred language
   */
  @JsonProperty("text")
  public String getText() {
    return text;
  }

  /**
   * Set fallback text of this message in the developer's preferred language.
   * @param value the fallback text.
   */
  @JsonProperty("text")
  public void setText(String value) {
    this.text = value;
  }

  /**
   * Fluent setter for the {@link #getText text property}.
   * @param text the fallback text
   * @return this object, for chaining.
   */
  public StatusMessage withText(String text) {
    setText(text);
    return this;
  }

  /**
   * Additional details such as a stack trace or native error code
   */
  @JsonProperty("detail")
  public Map<String, Object> getDetail() {
    return detail;
  }

  /**
   * Set additional detail property.
   * @param detail additional details to include with the message.
   */
  @JsonProperty("detail")
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public void setDetail(Map<String, Object> detail) {
    this.detail = detail;
  }

  /**
   * Fluent setter for the {@link #getDetail detail property}.
   * @param detail additional details to include with the message.
   * @return this object, for chaining.
   */
  public StatusMessage withDetail(Map<String, Object> detail) {
    setDetail(detail);
    return this;
  }

  /**
   * Adds the stack trace of the given Throwable to this message's "detail" map under the "stackTrace" key.  Note that
   * this modifies the existing detail Map, if there is one, so will only work if that map is writeable - if not this
   * method may throw an exception.
   */
  public StatusMessage withStackTrace(Throwable t) {
    if(detail == null) {
      detail = new HashMap<>();
    }
    detail.put("stackTrace", Stream.of(t.getStackTrace())
            .map(StackTraceElement::toString)
            .collect(Collectors.toList()));
    return this;
  }
}
