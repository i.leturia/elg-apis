/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.model.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import eu.elg.model.AnnotationObject;
import eu.elg.model.Response;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Response representing standoff annotations over a single stream of data (e.g. information extraction results).
 */
public class AnnotationsResponse extends Response<AnnotationsResponse> {
  private Map<String, List<AnnotationObject>> annotations;
  private Map<String, Object> features;

  /**
   * The annotations extracted.  The structure is a map where the key is a label for the type or group of annotations,
   * and the value is a list of individual annotations of the specified type.
   */
  @JsonProperty("annotations")
  public Map<String, List<AnnotationObject>> getAnnotations() {
    return annotations;
  }

  /**
   * Set the annotations for this response.
   *
   * @param annotations annotations map as described in {@link #getAnnotations()}
   */
  @JsonProperty("annotations")
  public void setAnnotations(Map<String, List<AnnotationObject>> annotations) {
    this.annotations = annotations;
  }

  /**
   * Fluent setter for the {@link #getAnnotations annotations property}
   *
   * @param annotations annotations map
   * @return this object, for chaining.
   */
  public AnnotationsResponse withAnnotations(Map<String, List<AnnotationObject>> annotations) {
    setAnnotations(annotations);
    return this;
  }

  /**
   * Shorthand to add annotations of a specific type to the response.  The provided list will <em>replace</em> any
   * existing list of annotations of the given type that are already contained in this response.
   *
   * @param type              annotation type (i.e. the label under which to add the annotations)
   * @param annotationsOfType the annotations to add
   * @return this object, for chaining
   */
  public AnnotationsResponse withAnnotations(String type, List<AnnotationObject> annotationsOfType) {
    if(annotations == null) {
      annotations = new LinkedHashMap<>();
    }
    annotations.put(type, annotationsOfType);
    return this;
  }

  /**
   * Features of this response as a whole.  The keys in the map must be strings, the values can be anything Jackson
   * knows how to serialise to JSON.
   */
  @JsonProperty("features")
  public Map<String, Object> getFeatures() {
    return features;
  }

  /**
   * Set features for this response as a whole. The keys in the map must be strings, the values can be anything Jackson
   * knows how to serialise to JSON.
   *
   * @param features map of features to set
   */
  @JsonProperty("features")
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  public void setFeatures(Map<String, Object> features) {
    this.features = features;
  }

  /**
   * Fluent setter for the {@link #getFeatures features property}
   *
   * @param features map of features to set
   * @return this object, for chaining.
   */
  public AnnotationsResponse withFeatures(Map<String, Object> features) {
    setFeatures(features);
    return this;
  }

  /**
   * Shorthand way to set features - this method takes an alternating list of key1, value1, key2, value2, etc.  Keys
   * must be strings, values can be anything Jackson knows how to serialise to JSON.
   *
   * @param keysAndValues alternating list of key, value, key, value, ...
   * @return this object, for chaining
   */
  public AnnotationsResponse withFeatures(Object... keysAndValues) {
    if(keysAndValues.length % 2 != 0) {
      throw new IllegalArgumentException("argument must be alternating keys and values");
    }
    Map<String, Object> featuresMap = new LinkedHashMap<>();
    for(int i = 0; i < keysAndValues.length; i += 2) {
      featuresMap.put((String) keysAndValues[i], keysAndValues[i + 1]);
    }
    return withFeatures(featuresMap);
  }

  /**
   * Convenience fluent setter for a single key/value feature pair.  This method calls {@link Map#put(Object, Object)
   * put} on the current {@link #getFeatures() features} map, creating a new {@link LinkedHashMap} if this is null.  If
   * the current features map is not mutable then this call may fail.
   *
   * @param key   the feature key
   * @param value the feature value (can by any type that Jackson knows how to serialise)
   * @return this object, for chaining.
   */
  public AnnotationsResponse withFeature(String key, Object value) {
    if(getFeatures() == null) {
      setFeatures(new LinkedHashMap<>());
    }
    getFeatures().put(key, value);
    return this;
  }

}
