/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.model.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import eu.elg.model.StandardMessages;
import eu.elg.model.StatusMessage;
import eu.elg.model.WarnForUnknownProperties;

import java.util.function.Function;

/**
 * Single class for a {@link ClassificationResponse}
 */
public class ClassificationClass extends WarnForUnknownProperties {

  private String className;

  private Number score;

  /**
   * The name of the classification class.
   */
  @JsonProperty("class")
  public String getClassName() {
    return className;
  }

  /**
   * Set the name of the classification class.
   *
   * @param className class name
   */
  @JsonProperty("class")
  public void setClassName(String className) {
    this.className = className;
  }

  /**
   * Fluent setter for the {@link #getClassName class name property}
   *
   * @param className class name
   * @return this object, for chaining.
   */
  public ClassificationClass withClassName(String className) {
    setClassName(className);
    return this;
  }

  /**
   * Score giving some indication of the classifier's confidence in this prediction.  These scores are not assumed to
   * have any intrinsic meaning beyond providing a way to order multiple classification options within the same {@link
   * ClassificationResponse}, in particular they are not assumed to be probabilities (0 to 1) or percentages (0 to 100),
   * or even necessarily positive numbers.
   */
  @JsonProperty("score")
  public Number getScore() {
    return score;
  }

  /**
   * Set the confidence score for this classification.
   *
   * @param score confidence score.
   */
  @JsonProperty("score")
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public void setScore(Number score) {
    this.score = score;
  }

  /**
   * Fluent setter for the {@link #getScore score property}
   * @param score confidence score
   * @return this object, for chaining.
   */
  public ClassificationClass withScore(Number score) {
    setScore(score);
    return this;
  }

  @Override
  protected Function<String, StatusMessage> propertyNameToMessage() {
    return StandardMessages::elgResponseClassificationPropertyUnsupported;
  }
}
