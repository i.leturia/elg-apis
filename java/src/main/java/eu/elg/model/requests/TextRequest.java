/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.model.requests;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import eu.elg.model.Markup;
import eu.elg.model.Request;
import eu.elg.model.StatusMessage;

import java.util.List;

/**
 * Request representing a single piece of text, optionally with associated markup.
 */
public class TextRequest extends Request<TextRequest> {
  private String content;
  private String mimeType;
  private Markup markup;

  /**
   * The text, directly inline within the JSON request.
   */
  @JsonProperty("content")
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public String getContent() {
    return content;
  }

  /**
   * Set the text content
   *
   * @param content text content, to include directly inline within this request
   */
  @JsonProperty("content")
  public void setContent(String content) {
    this.content = content;
  }

  /**
   * Fluent setter for the {@link #getContent content property}
   *
   * @param content the text content
   * @return this object, for chaining.
   */
  public TextRequest withContent(String content) {
    setContent(content);
    return this;
  }

  /**
   * MIME type identifying the content, for example if the content is HTML this might be text/html
   */
  @JsonProperty("mimeType")
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public String getMimeType() {
    return mimeType;
  }

  /**
   * Set the MIME type for the content.
   *
   * @param mimeType MIME type
   */
  @JsonProperty("mimeType")
  public void setMimeType(String mimeType) {
    this.mimeType = mimeType;
  }

  /**
   * Fluent setter for the {@link #getMimeType MIME type property}
   *
   * @param mimeType MIME type
   * @return this object, for chaining
   */
  public TextRequest withMimeType(String mimeType) {
    setMimeType(mimeType);
    return this;
  }

  /**
   * The markup (annotations and features) on this text.
   *
   * @return the current markup.
   */
  @JsonUnwrapped
  public Markup getMarkup() {
    return markup;
  }

  /**
   * Set the markup on this text.
   *
   * @param markup the markup to use.
   */
  @JsonUnwrapped
  public void setMarkup(Markup markup) {
    this.markup = markup;
  }

  /**
   * Fluent setter for the {@link #getMarkup() markup} property.
   *
   * @param markup the markup to use
   * @return this object, for chaining
   */
  public TextRequest withMarkup(Markup markup) {
    setMarkup(markup);
    return this;
  }

  public String type() {
    return "text";
  }

  @Override
  protected boolean validPropertyName(String propName) {
    return Markup.validPropertyName(propName);
  }

}
