/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.model.requests;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import eu.elg.model.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

/**
 * Request representing text with some structure, for example a list of paragraphs or sentences, or a corpus of
 * documents, each divided into sentences.  While this could be represented as standoff annotations in a plain "text"
 * request, the structured format is more suitable for certain types of tools.
 */
public class StructuredTextRequest extends Request<StructuredTextRequest> {
  private List<Text> texts;

  /**
   * The texts in this request.
   */
  @JsonProperty("texts")
  public List<Text> getTexts() {
    return texts;
  }

  /**
   * Set the texts in this request.
   *
   * @param texts texts to return
   */
  @JsonProperty("texts")
  public void setTexts(List<Text> texts) {
    this.texts = texts;
  }

  /**
   * Fluent setter for the {@link #getTexts texts property}
   *
   * @param texts texts to return
   * @return this object, for chaining
   */
  public StructuredTextRequest withTexts(List<Text> texts) {
    setTexts(texts);
    return this;
  }

  /**
   * Fluent setter for the {@link #getTexts texts property} taking multiple arguments rather than a single list, to
   * support constructs like <code>new StructuredTextRequest().withTexts(new Text()....)</code>
   *
   * @param texts texts to return
   * @return this object, for chaining
   */
  public StructuredTextRequest withTexts(Text... texts) {
    return withTexts(Arrays.asList(texts));
  }


  /**
   * A single node in a structured text request.  Each text can have an associated score (for example a confidence value
   * for multiple alternative translations or transcriptions) and optional annotations, which can be linked to both the
   * result text in this object and to the original source material from the corresponding request.
   */
  public static class Text extends WarnForUnknownProperties {
    private String content;
    private String mimeType;
    private List<Text> texts;
    private Markup markup = new Markup();

    /**
     * The text, directly inline within the JSON request.
     */
    @JsonProperty("content")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getContent() {
      return content;
    }

    /**
     * Set the text content
     *
     * @param content text content, to include directly inline within this request
     */
    @JsonProperty("content")
    public void setContent(String content) {
      this.content = content;
    }

    /**
     * Fluent setter for the {@link #getContent content property}
     *
     * @param content the text content
     * @return this object, for chaining.
     */
    public Text withContent(String content) {
      setContent(content);
      return this;
    }

    /**
     * MIME type identifying the content, for example if the content is HTML this might be text/html
     */
    @JsonProperty("mimeType")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getMimeType() {
      return mimeType;
    }

    /**
     * Set the MIME type for the content.
     *
     * @param mimeType MIME type
     */
    @JsonProperty("mimeType")
    public void setMimeType(String mimeType) {
      this.mimeType = mimeType;
    }

    /**
     * Fluent setter for the {@link #getMimeType MIME type property}
     *
     * @param mimeType MIME type
     * @return this object, for chaining
     */
    public Text withMimeType(String mimeType) {
      setMimeType(mimeType);
      return this;
    }

    /**
     * The texts in this part of the request.
     */
    @JsonProperty("texts")
    public List<Text> getTexts() {
      return texts;
    }

    /**
     * Set the texts in this part of the request.
     *
     * @param texts texts to return
     */
    @JsonProperty("texts")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public void setTexts(List<Text> texts) {
      this.texts = texts;
    }

    /**
     * Fluent setter for the {@link #getTexts texts property}
     *
     * @param texts texts to return
     * @return this object, for chaining
     */
    public Text withTexts(List<Text> texts) {
      setTexts(texts);
      return this;
    }

    /**
     * Fluent setter for the {@link #getTexts texts property} taking multiple arguments rather than a single list, to
     * support constructs like <code>new Text().withTexts(new Text()....)</code>
     *
     * @param texts texts to return
     * @return this object, for chaining
     */
    public Text withTexts(Text... texts) {
      return withTexts(Arrays.asList(texts));
    }

    /**
     * The markup (annotations and features) on this text.
     *
     * @return the current markup.
     */
    @JsonUnwrapped
    public Markup getMarkup() {
      return markup;
    }

    /**
     * Set the markup on this text.
     *
     * @param markup the markup to use.
     */
    @JsonUnwrapped
    public void setMarkup(Markup markup) {
      this.markup = markup;
    }

    /**
     * Fluent setter for the {@link #getMarkup() markup} property.
     *
     * @param markup the markup to use
     * @return this object, for chaining
     */
    public Text withMarkup(Markup markup) {
      setMarkup(markup);
      return this;
    }

    @Override
    protected Function<String, StatusMessage> propertyNameToMessage() {
      return StandardMessages::elgRequestStructuredTextPropertyUnsupported;
    }

    @Override
    public List<StatusMessage> unknownPropertyWarnings() {
      return combineUnknownPropertyWarnings(super.unknownPropertyWarnings(), texts);
    }

    @Override
    protected boolean validPropertyName(String propName) {
      return Markup.validPropertyName(propName);
    }
  }

  public String type() {
    return "structuredText";
  }

  @Override
  public List<StatusMessage> unknownPropertyWarnings() {
    return combineUnknownPropertyWarnings(super.unknownPropertyWarnings(), texts);
  }
}
