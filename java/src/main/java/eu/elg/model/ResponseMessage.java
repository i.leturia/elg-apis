/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Message representing a response from an ELG service, which may be successful completion, failed completion, or
 * partial progress.
 */
public class ResponseMessage {

  /**
   * Response for successful responses.
   */
  private Response<?> response;

  /**
   * For error responses.
   */
  private Failure failure;

  /**
   * For information progress responses.
   */
  private Progress progress;

  /**
   * Response data, for successful completion messages.
   */
  @JsonProperty("response")
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public Response<?> getResponse() {
    return response;
  }

  /**
   * Set the response data for a successful completion.
   *
   * @param response response data.
   */
  @JsonProperty("response")
  public void setResponse(Response<?> response) {
    this.response = response;
  }

  /**
   * Failure details, for a failed completion message.
   */
  @JsonProperty("failure")
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public Failure getFailure() {
    return failure;
  }

  /**
   * Set the failure details, for a failed completion message.
   *
   * @param failure failure details
   */
  @JsonProperty("failure")
  public void setFailure(Failure failure) {
    this.failure = failure;
  }

  /**
   * Progress report, for a partial progress message.
   */
  @JsonProperty("progress")
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public Progress getProgress() {
    return progress;
  }

  /**
   * Set progress report, for a partial progress message.
   *
   * @param progress progress report.
   */
  @JsonProperty("progress")
  public void setProgress(Progress progress) {
    this.progress = progress;
  }

  /**
   * Take a list of warning messages and add them where possible to this response message - as additional errors if it's
   * a failure, as warnings to the response if it's a response.
   *
   * @param warnings the warnings we want to add.
   */
  public void addWarnings(List<StatusMessage> warnings) {
    if(warnings == null || warnings.isEmpty()) {
      return;
    }

    if(failure != null) {
      // this is a failure, add the warnings to the failure errors
      List<StatusMessage> failureErrors = new ArrayList<>(failure.getErrors());
      failureErrors.addAll(warnings);
      failure.setErrors(failureErrors);
    } else if(response != null) {
      List<StatusMessage> responseWarnings = response.getWarnings();
      if(responseWarnings == null) {
        response.setWarnings(warnings);
      } else {
        responseWarnings = new ArrayList<>(responseWarnings);
        responseWarnings.addAll(warnings);
        response.setWarnings(responseWarnings);
      }
    } else if(progress != null) {
      // progress message, not a lot we can do
      if(progress.getMessage() == null) {
        progress.setMessage(warnings.get(0));
      }
    }
  }
}
