/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Progress information, for a partial progress message
 */
public class Progress {

  private Double percent;

  private StatusMessage message;

  /**
   * Percentage completion.  Optional, but if specified it should be between 0.0 and 100.0.
   */
  @JsonProperty("percent")
  public Double getPercent() {
    return percent;
  }

  /**
   * Set percentage completion for this progress report.
   *
   * @param percent the completion percentage, which should be either null (no information) or a number between 0.0 and
   *                100.0.
   */
  @JsonProperty("percent")
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public void setPercent(Double percent) {
    this.percent = percent;
  }

  /**
   * Fluent setter for the {@link #getPercent percent property}
   * @param percent the completion percentage, which should be either null (no information) or a number between 0.0 and
   *                100.0.
   * @return this object, for chaining
   */
  public Progress withPercent(Double percent) {
    setPercent(percent);
    return this;
  }

  /**
   * An optional status message describing this progress report.
   */
  @JsonProperty("message")
  public StatusMessage getMessage() {
    return message;
  }

  /**
   * Set the optional status message for this progress report.
   * @param message status message
   */
  @JsonProperty("message")
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public void setMessage(StatusMessage message) {
    this.message = message;
  }

  /**
   * Fluent setter for the {@link #getMessage message property}.
   * @param message status message
   * @return this object, for chaining.
   */
  public Progress withMessage(StatusMessage message) {
    setMessage(message);
    return this;
  }

  /**
   * Wrap this response in a message.
   * @return a {@link ResponseMessage} wrapping this response
   */
  public ResponseMessage asMessage() {
    ResponseMessage msg = new ResponseMessage();
    msg.setProgress(this);
    return msg;
  }
}
