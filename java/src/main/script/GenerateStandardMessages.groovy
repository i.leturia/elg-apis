/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

// This script generates the eu.elg.model.StandardMessages class from
// elg-messages.properties

import org.apache.commons.text.StringEscapeUtils as SEU

def props = new Properties()
new File(project.basedir, "src/main/resources/eu/elg/elg-messages.properties").withReader("UTF-8") { r ->
  props.load(r)
}

File genSrcDir = new File(project.basedir, "target/generated-sources/java")
File modelDir = new File(genSrcDir, "eu/elg/model")
modelDir.mkdirs()
new File(modelDir, "StandardMessages.java").withPrintWriter("UTF-8") { w ->
  w << '''\
/* THIS IS A GENERATED FILE - DO NOT EDIT */

/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.model;

import java.util.Objects;

/**
 * <p>
 * This class provides easy access to the standard set of ELG status messages
 * that are provided by default by the platform and should be fully translated
 * in the ELG user interface.  If you use codes other than these standard ones
 * in your services then you should also try to contribute translations of your
 * messages into as many languages as possible for the benefit of other ELG
 * users.
 * </p>
 *
 * <p>
 * <strong>Implementation note:</strong> This class is auto-generated from
 * <code>elg-messages.properties</code> - to add new message codes you should
 * edit the property files, not this class directly.
 * </p>
 */
public class StandardMessages {
  /**
   * This class should not be instantiated.
   */
  private StandardMessages() {}

'''

  props.keySet().sort().each { k ->
    // work out the number of args
    def args = props[k].findAll(/\{(\d+)/) { all, num -> num.toInteger() + 1 }.max() ?: 0
    // convert the dotted name to camel case
    def camelCaseName = k.replaceAll(/[.-](.?)/) { match, letter -> letter.toUpperCase() }
    def argsDecl = (0..<args).collect { "Object arg$it" }.join(', ')
    def argsValues = (0..<args).collect { "Objects.toString(arg$it)" }.join(', ')
    w << """\

  /**
   * ${SEU.escapeHtml3(props[k])}
   */
  public static StatusMessage ${camelCaseName}(${argsDecl}) {
    return new StatusMessage().withCode("${SEU.escapeJava(k)}")
                              .withText("${SEU.escapeJava(props[k])}")"""
    if(args > 0) {
      w << """
                              .withParams(${argsValues});"""
    } else {
      w << ";"
    }
    w << """
  }
"""
  }

  w << """

}
"""
}

project.addCompileSourceRoot(genSrcDir.getAbsolutePath())
