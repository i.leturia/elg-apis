#
#   Copyright 2019 The European Language Grid
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# This file contains the standard ELG status messages, translations should
# be placed in files named elg-messages_LANG.properties alongside this file.
#

# general bad request errors
elg.request.invalid=Ogiltig förfrågan
elg.request.missing=Ingen förfrågan angiven i meddelande
elg.request.type.unsupported=Förfrågan av typ {0} stöds ej av denna tjänst
elg.request.property.unsupported=Otillåten parameter {0} i förfrågan

elg.request.too.large=Förfrågans storlek överskrider maxvärdet

# Errors specific to text requests
elg.request.text.mimeType.unsupported=MIME av typ {0} stöds ej av denna tjänst

# Errors specific to audio requests
elg.request.audio.format.unsupported=Ljudformat {0} stöds ej av denna tjänst
elg.request.audio.sampleRate.unsupported=Ljudsamplingshastighet {0} stöds ej av denna tjänst

# Errors specific to image requests
elg.request.image.format.unsupported=Bildformat {0} stöds ej av denna tjänst

# Errors specific to structured text requests
elg.request.structuredText.property.unsupported=Otillåten parameter {0} i "texts" i structuredText-förfrågan

# General bad response errors
elg.response.invalid=Ogitigt svarsmeddelande
elg.response.type.unsupported=Svarstyp {0} stöds ej

# Unknown property in response
elg.response.property.unsupported=Otillåten parameter {0} i svar
elg.response.texts.property.unsupported=Otillåten parameter {0} i svar från "texts"
elg.response.classification.property.unsupported=Otillåten parameter {0} i "classes" i svar från "classification"

# User requested a service that does not exist
elg.service.not.found=Tjänst {0} hittades ej
elg.async.call.not.found=Async-anrop {0} hittades ej

# Permission problems
elg.permissions.quotaExceeded=Tillåten kvot överskriden
elg.permissions.accessDenied=Nekat tillträde
elg.permissions.accessManagerError=Fel i access manager: {0}

# Temporary file storage service
elg.file.not.found=Fil {0} hittades ej
elg.file.expired=Efterfrågad fil {0} inte längre tillgänglig
elg.upload.too.large=Uppladdning överskrider maxstorlek

# generic internal error when there's no more specific option
elg.service.internalError=Internt fel under bearbetning: {0}
