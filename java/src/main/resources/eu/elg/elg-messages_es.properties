#
#   Copyright 2019 The European Language Grid
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# This file contains the standard ELG status messages, translations should
# be placed in files named elg-messages_LANG.properties alongside this file.
#

# general bad request errors
elg.request.invalid=Mensaje de petición inválido
elg.request.missing=Ninguna petición provista en el mensaje
elg.request.type.unsupported=Tipo de petición {0} no soportada por este servicio
elg.request.property.unsupported=Propiedad no soportada {0} en la petición

elg.request.too.large=Tamaño de petición demasiado grande

# Errors specific to text requests
elg.request.text.mimeType.unsupported=Tipo MIME {0} no soportado por este servicio

# Errors specific to audio requests
elg.request.audio.format.unsupported=Formato de audio {0} no soportado por este servicio
elg.request.audio.sampleRate.unsupported=Tasa de sampleo de audio {0} no soportado por este servicio

# Errors specific to image requests
elg.request.image.format.unsupported=Formato de imagen {0} no soportado por este servicio

# Errors specific to structured text requests
elg.request.structuredText.property.unsupported=Propiedad no soportada {0} en "texts" de la petición structuredText

# General bad response errors
elg.response.invalid=Mensaje de respuesta inválido
elg.response.type.unsupported=Tipo de respuesta {0} no soportada

# Unknown property in response
elg.response.property.unsupported=Propiedad no soportada {0} en la respuesta
elg.response.texts.property.unsupported=Propiedad no soportada {0} en "texts" de la respuesta de textos
elg.response.classification.property.unsupported=Propiedad no soportada {0} en "classes" de la respuesta de clasificación

# User requested a service that does not exist
elg.service.not.found=Servicio {0} no se encontró
elg.async.call.not.found=Llamada asíncrona {0} no encontrada

# Permission problems
elg.permissions.quotaExceeded=Límite autorizado excedido
elg.permissions.accessDenied=Acceso denegado
elg.permissions.accessManagerError=Error en el gestor de acceso: {0}

# Temporary file storage service
elg.file.not.found=Archivo {0} no encontrado
elg.file.expired=Archivo solicitado {0} ya no disponible
elg.upload.too.large=Carga de archivo demasiado grande

# generic internal error when there's no more specific option
elg.service.internalError=Error interno durante el procesamiento: {0}
